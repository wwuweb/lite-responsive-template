# Get Started with the Western Template

1. Put lite-template files into your file share folder (if on campus web).
2. Open in Dreamweaver and use Manage Sites so your site is recognized by Dreamweaver.
3. Find and Replace (CTRL/CMD + F) 'lite-template' to your site's share folder name (such as webtech, or president, or physics). Select 'Entire Current Local Site' under 'Find In'. Be sure all files are closed at this point to prevent Dreamweaver from being confused.
4. Find and Replace 'Western Responsive Lite Template' to your site's name (Such as Department of Design, or Office of Communications).

# Customize

1. Inside of the 'customize' folder you modify the site's banner image, site name, CSS, and navigation.
2. Open site-name.html and verify the site's title. This is the site's title located on top of the banner image of every page.
3. Modify the site's banner image in Photoshop with banner-image.psd. When you are finished, save the .psd and save-as banner-image.jpg to replace the current .jpg file in the customize folder. This will update your site's banner on every page.
4. If you would prefer to place your site name's text directly in the banner image, go to customize.css and

# Creating a New Page

* Change "Page Name" in the <title> to whatever your page's name is. This should match the <h1> (top heading). Avoid redundant titles that include your site's name.
* The Example Pages (such as, Staff & Faculty, Contact, Gallery) are available for common page types for you to modify.

# Changes

* Related Links in the main navigation area is no longer supported. The modern nature of the internet means users can easily search for websites and each page does not need to be a portal. We don't need to manually create the "web" of the internet.

# Best Practices

* Content and user first.
* Text no wider than 65 to 70 characters
* Be aware the web and how users look at websites is fluid. Your page might look slightly different to everyone who sees it, so emphasize the important things (clear content) before spending hours on visual details.